-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 28 fév. 2023 à 11:25
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `supersavers`
--

-- --------------------------------------------------------

--
-- Structure de la table `hero`
--

CREATE TABLE `hero` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `hero`
--

INSERT INTO `hero` (`id`, `name`, `latitude`, `longitude`, `phone`) VALUES
(1, 'Batman', '48.36491749617739', '2.388388655066707', '06 78 96 54 12'),
(2, 'Wonderwoman', '45.73062898264198', '4.85073805265707', '06 78 10 42 56'),
(3, 'Aquaman', '42.7727232601688', '4.982649627527955', '06 85 25 45 65'),
(4, 'Flash', '43.31868363907222', '2.7841233796794467', '06 88 77 44 55 '),
(5, 'Superman', '47.92508510314007', '2.8280939046364093', '06 23 12 45 56'),
(6, 'Catwoman', '45.76129581948936', '1.2891255311424124', '06 79 46 13 82'),
(7, 'Thor', '48.189436797281516', '-2.1845459404582606', '06 78 45 12 32'),
(8, 'Scarlet Witch', '46.73374289048229', '2.3004476051527427', '06 85 25 25 85'),
(9, 'Supergirl', '44.49013784905748', '3.839415978646739', '06 23 12 45 56'),
(10, 'Elektra', '48.82992159521573', '0.05795083234723109', '06 79 89 78 45'),
(11, 'Black Widow', '45.57704207966763', '3.7514749287327747', '06 78 12 32 59');

-- --------------------------------------------------------

--
-- Structure de la table `hero_manage_incidents`
--

CREATE TABLE `hero_manage_incidents` (
  `id` int(11) NOT NULL,
  `heroId` int(11) NOT NULL,
  `incidentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `hero_manage_incidents`
--

INSERT INTO `hero_manage_incidents` (`id`, `heroId`, `incidentId`) VALUES
(1, 1, 4),
(2, 1, 9),
(3, 1, 10),
(4, 2, 2),
(5, 2, 5),
(6, 2, 8),
(7, 3, 1),
(8, 3, 3),
(9, 4, 6),
(10, 4, 7),
(11, 5, 4),
(12, 5, 10),
(13, 6, 9),
(14, 6, 10),
(15, 7, 1),
(16, 7, 6),
(17, 8, 2),
(18, 8, 3),
(19, 8, 8),
(20, 9, 1),
(21, 9, 5),
(22, 9, 7),
(23, 10, 2),
(24, 10, 4),
(25, 10, 6),
(26, 11, 7),
(27, 11, 8),
(28, 11, 9);

-- --------------------------------------------------------

--
-- Structure de la table `incidents`
--

CREATE TABLE `incidents` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `imageUrl` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `incidents`
--

INSERT INTO `incidents` (`id`, `type`, `imageUrl`) VALUES
(1, 'Incendie', 'fire1.1.png'),
(2, 'Accident routier', 'roadAccident.png'),
(3, 'Accident fluvial', 'boatAccident.png'),
(4, 'Accident aérien', 'airAccident.png'),
(5, 'Eboulement', 'landSlide1.1.png'),
(6, 'Invasion de serpent', 'snake.png'),
(7, 'Fuite de gaz', 'gasLeak.png'),
(8, 'Manifestation', 'manifestation1.1.png'),
(9, 'Braquage', 'holdup.png'),
(10, 'Evasion prisonnier', 'escape1.1.png');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `hero`
--
ALTER TABLE `hero`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `hero_manage_incidents`
--
ALTER TABLE `hero_manage_incidents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idhero` (`heroId`),
  ADD KEY `idincident` (`incidentId`);

--
-- Index pour la table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `hero`
--
ALTER TABLE `hero`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `hero_manage_incidents`
--
ALTER TABLE `hero_manage_incidents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `hero_manage_incidents`
--
ALTER TABLE `hero_manage_incidents`
  ADD CONSTRAINT `hero_manage_incidents_ibfk_1` FOREIGN KEY (`heroId`) REFERENCES `hero` (`id`),
  ADD CONSTRAINT `hero_manage_incidents_ibfk_2` FOREIGN KEY (`incidentId`) REFERENCES `incidents` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

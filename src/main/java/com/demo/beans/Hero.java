package com.demo.beans;

import java.util.ArrayList;
import java.util.List;

public class Hero {
    private String name;
    private String latitude;
    private String longitude;
    private String phone;
    private List<Incident> incidentsManaged = new ArrayList<>();

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Incident> getIncidentsManaged() {
        return incidentsManaged;
    }
    public void setIncidentsManaged(List<Incident> incidents) {
        this.incidentsManaged = incidents;
    }

}

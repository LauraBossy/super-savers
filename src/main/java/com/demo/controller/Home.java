package com.demo.controller;

import com.demo.beans.Hero;
import com.demo.dao.DaoFactory;
import com.demo.dao.HeroDao;
import com.demo.dao.IncidentDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "Home", value = "/")
public class Home extends HttpServlet {

    private IncidentDao incidentDao;

    public void init() throws ServletException {
        // Crée une instance de DaoFactory (connexion à la base de données)
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.incidentDao = daoFactory.getIncidentDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("incidents", incidentDao.getIncidents());
        this.getServletContext().getRequestDispatcher("/WEB-INF/view/firstPage.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}

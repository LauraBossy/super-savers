package com.demo.controller;

import com.demo.dao.DaoFactory;
import com.demo.dao.HeroDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "HeroSearch", value = "/HeroSearch")
public class HeroSearch extends HttpServlet {

    private HeroDao heroDao;

    public void init() throws ServletException {
        // Crée une instance de DaoFactory (connexion à la base de données)
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.heroDao = daoFactory.getHeroDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("heroes", heroDao.getHeroesByIncidentId(Integer.parseInt(request.getParameter("incidentId" ))));
        request.setAttribute("incidentType", request.getParameter("incidentType"));
        this.getServletContext().getRequestDispatcher("/WEB-INF/view/heroSearch.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("latitude", request.getParameter("latitude"));
        request.setAttribute("longitude", request.getParameter("longitude"));
        this.getServletContext().getRequestDispatcher("/WEB-INF/view/heroSearch.jsp").forward(request, response);
    }
}

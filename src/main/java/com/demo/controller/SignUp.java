package com.demo.controller;

import com.demo.beans.Hero;
import com.demo.beans.Incident;
import com.demo.dao.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "SignUp", value = "/SignUp")
public class SignUp extends HttpServlet {

    private HeroDao heroDao;
    private IncidentDao incidentDao;

    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.heroDao = daoFactory.getHeroDao();
        this.incidentDao = daoFactory.getIncidentDao();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("incidents", incidentDao.getIncidents());
        this.getServletContext().getRequestDispatcher("/WEB-INF/view/registerForm.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Hero hero = new Hero();
        hero.setName(request.getParameter("name"));
        hero.setLatitude(request.getParameter("latitude"));
        hero.setLongitude(request.getParameter("longitude"));
        hero.setPhone(request.getParameter("phone"));

        List<Incident> incidents = incidentDao.getIncidents();

        // Pour chaque incident i de ma liste d'incident...
        for (Incident i : incidents) {
            // ... si dans ma requête j'ai un paramètre "incident_l'id-de-l'incident-i" ...
            if (request.getParameter("incident_" + i.getId()) != null) {
                // ... alors j'ajoute l'incident i dans la liste des incidents gérés par mon héro
                hero.getIncidentsManaged().add(i);
            }
        }

        if (!heroDao.doesMyHeroExist(hero)) {
            heroDao.addHero(hero);
            request.setAttribute("message", "Vous êtes bien inscrit " + hero.getName());
        }
        else {
            request.setAttribute("message", "Vous êtes déjà inscrit");
        }

        this.getServletContext().getRequestDispatcher("/WEB-INF/view/validationRegister.jsp").forward(request, response);
    }
}

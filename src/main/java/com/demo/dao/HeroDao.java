package com.demo.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.demo.beans.Hero;
import com.demo.beans.Incident;

public class HeroDao implements IHeroDao {

    /**
     * Créer un objet de type DaoFactory, je pourrai donc utiliser les méthodes ce type
     */
    private DaoFactory daoFactory;
    private List<Incident> getHeroStrengths(int heroId) throws SQLException {
        List<Incident> incidents = new ArrayList<>();

        Connection connexion = daoFactory.getConnection();
        Statement statement = connexion.createStatement();

        // Exécution de la requête
        ResultSet resultat = statement.executeQuery("SELECT i.id, i.type FROM hero_manage_incidents as rel, incidents as i WHERE i.id = rel.incidentId AND rel.heroId = " + heroId);

        while (resultat.next()) {
            int id = resultat.getInt("id");
            String type = resultat.getString("type");

            Incident incident = new Incident();
            incident.setId(id);
            incident.setType(type);

            incidents.add(incident);
        }

        return incidents;
    }

    /**
     * Constructeur, qui attribue à l'objet daoFactory de cette classe la daoFactory que je lui passe en paramètre)
     * @param daoFactory
     */
    HeroDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    /**
     * Implémente la méthode addHero de l'interface IHeroDao, donc l'override
     * @param hero
     */
    @Override
    public void addHero(Hero hero) {

            try {
                Connection connexion = daoFactory.getConnection();
                PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO hero(name, latitude, longitude, phone) VALUES(?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, hero.getName());
                preparedStatement.setString(2, hero.getLatitude());
                preparedStatement.setString(3, hero.getLongitude());
                preparedStatement.setString(4, hero.getPhone());

                preparedStatement.executeUpdate();

                ResultSet ids = preparedStatement.getGeneratedKeys();
                ids.next();
                int heroId = ids.getInt(1);

                /**
                 * récupère la liste des incidents gérés par le héro dans Hero.java et boucle une insertion SQL dessus pour renseigner la table de matching
                 */
                for(Incident i : hero.getIncidentsManaged()) {
                    preparedStatement = connexion.prepareStatement("INSERT INTO hero_manage_incidents(heroId, incidentId) VALUES(?, ?);");
                    preparedStatement.setInt(1, heroId);
                    preparedStatement.setInt(2, i.getId());
                    preparedStatement.executeUpdate();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

    }

    @Override
    public List<Hero> getHeroes() {
        List<Hero> heroes = new ArrayList<Hero>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();

            // Exécution de la requête
            resultat = statement.executeQuery("SELECT id, name, latitude, longitude, phone FROM hero;");

            // Récupération des données
            while (resultat.next()) {
                int id = resultat.getInt("id");
                String name = resultat.getString("name");
                String latitude = resultat.getString("latitude");
                String longitude = resultat.getString("longitude");
                String phone = resultat.getString("phone");

                Hero hero = new Hero();
                hero.setName(name);
                hero.setLatitude(latitude);
                hero.setLongitude(longitude);
                hero.setPhone(phone);

                hero.setIncidentsManaged(getHeroStrengths(id));
                heroes.add(hero);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }

        return heroes;
    }

    @Override
    public List<Hero> getHeroesByIncidentId(int incidentId) {
        List<Hero> heroesByIncident = new ArrayList<Hero>();

        for (Hero h : getHeroes()) {

            for (Incident i : h.getIncidentsManaged()) {
                if (i.getId() == incidentId) {
                    heroesByIncident.add(h);
                }
            }
        }
        return heroesByIncident;
    }

    public Boolean doesMyHeroExist(Hero hero) {
        for (Hero h : getHeroes()) {
            if (h.getName().equals(hero.getName()))
                return true;
        }
        return false;
    }
}
package com.demo.dao;

import java.util.List;

import com.demo.beans.Hero;

public interface IHeroDao {
    void addHero(Hero hero);
    List<Hero> getHeroes();
    List<Hero> getHeroesByIncidentId(int incidentId);
}
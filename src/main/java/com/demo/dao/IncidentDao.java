package com.demo.dao;

import com.demo.beans.Incident;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class IncidentDao implements IIncidentDao {

    /**
     * Créer un objet de type DaoFactory, je pourrai donc utiliser les méthodes ce type
     */
    private DaoFactory daoFactory;

    /**
     * Constructeur, qui attribue à l'objet daoFactory de cette classe la daoFactory que je lui passe en paramètre)
     * @param daoFactory
     */
    IncidentDao(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    /**
     * Implémentation de la méthode getIncidents de l'interface IIncidentDao, donc l'override
     * @return
     */
    @Override
    public List<Incident> getIncidents() {
        List<Incident> incidents = new ArrayList<Incident>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();

            // Exécution de la requête
            resultat = statement.executeQuery("SELECT id, type, imageUrl FROM incidents;");

            // Récupération des données
            while (resultat.next()) {

                int id = resultat.getInt("id");
                String type = resultat.getString("type");
                String imageUrl = resultat.getString("imageUrl");

                Incident incident = new Incident();
                incident.setId(id);
                incident.setType(type);
                incident.setImageUrl(imageUrl);

                incidents.add(incident);

            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }

        return incidents;
    }
}

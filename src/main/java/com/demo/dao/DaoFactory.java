package com.demo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class  DaoFactory {
    private String url;
    private String username;
    private String password;

    /**
     * Constructeur, créé l'url pour se connecter à la base de donnée
     * @param url
     * @param username
     * @param password
     */
    DaoFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * @return une instance de DaoFactory
     * C'est static donc je l'appelle qu'une fois, et ensuite si je le re-appelle ça utilisera celui qui est déjà actif
     * Class.forName(<Classe>) charge la classe spécifiée (ici Driver, qui charge le driver qui gère la connexion)
     */
    public static DaoFactory getInstance() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {

        }

        DaoFactory instance = new DaoFactory(
                "jdbc:mysql://localhost:3306/supersavers", "root", "");
        return instance;
    }

    /**
     * @return la connexion à la base de données
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * Retourne les Dao pour y accéder à partir de la Factory, afin de ne passer que par cette Factory (pour que ce soit bien rangé)
     * @return
     */
    // Lorsque j'instancierai un HeroDao, je lui passerai la DaoFactory courante ici en paramètre
    public HeroDao getHeroDao() {
        return new HeroDao(this);
    }
    // Lorsque j'instancierai un IncidentDao, je lui passerai la DaoFactory courante ici en paramètre
    public IncidentDao getIncidentDao() {
        return new IncidentDao(this);
    }
}
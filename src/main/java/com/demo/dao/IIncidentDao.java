package com.demo.dao;

import java.util.List;

import com.demo.beans.Incident;

public interface IIncidentDao {
    List<Incident> getIncidents();
}
<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
  <head>
      <title> Accueil - SuperSavers </title>
      <meta charset="utf-8"/>
      <link rel="stylesheet" href="/css/bootstrap.min.css">
      <link rel="stylesheet" href="/css/tooplate-style.css">
  </head>
  <body>
      <header class="tm-site-header">
            <h1 class="tm-site-name"> SuperSavers </h1>
            <nav class="navbar navbar-expand-md tm-main-nav-container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#tmMainNav" aria-controls="tmMainNav" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse tm-main-nav" id="tmMainNav">
                    <ul class="nav nav-fill tm-main-nav-ul">
                        <li class="nav-item"><a class="nav-link" href="/"> Accueil </a></li>
                        <li class="nav-item"><a class="nav-link" href="/SignUp"> Proposer mes services </a></li>
                    </ul>
                </div>
            </nav>
      </header>
      <div class="container">
          <div class="tm-main-content">
              <section class="tm-margin-b-l">
                  <p> Besoin d&#39aide d&#39urgence ? Indiquez-nous le type d&#39incident. </p>
                  <div class="tm-gallery">
                      <div class="row">
                          <c:forEach var="incident" items="${ incidents }">
                                <figure class="col-lg-3 col-md-4 col-sm-6 col-12 tm-gallery-item">
                                    <a href="/HeroSearch?incidentId=${ incident.id }&incidentType=${ incident.type }">
                                        <p class="tm-figcaption">
                                            <img src="images/${ incident.imageUrl }" alt="Image" class="img-fluid tm-img-center">
                                            <c:out value="${ incident.type }" />
                                        </p>
                                    </a>
                                </figure>
                          </c:forEach>
                      </div>
                  </div>
              </section>
          </div>
      </div>
  </body>
</html>

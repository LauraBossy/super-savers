<%@ page pageEncoding="UTF-8" %>

<header class="tm-site-header">
      <h1 class="tm-site-name"> SuperSavers </h1>

      <nav class="navbar navbar-expand-md tm-main-nav-container">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#tmMainNav" aria-controls="tmMainNav" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fa fa-bars"></i>
          </button>

          <div class="collapse navbar-collapse tm-main-nav" id="tmMainNav">
              <ul class="nav nav-fill tm-main-nav-ul">
                  <li class="nav-item"><a class="nav-link active" href="index.html"> Accueil </a></li>
                  <li class="nav-item"><a class="nav-link" href="/SignIn"> Accéder à mon espace de super-héros </li></a>
                  <li class="nav-item"><a class="nav-link" href="/SignUp"> Proposer mes services </a></li>
              </ul>
          </div>
      </nav>
</header>
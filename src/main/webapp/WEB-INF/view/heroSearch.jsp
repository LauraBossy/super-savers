<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
  <head>
      <title> Déclaration d&#39un incident </title>
      <meta charset="utf-8"/>
      <link rel="stylesheet" href="/css/bootstrap.min.css">
      <link rel="stylesheet" href="/css/tooplate-style.css">
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
      <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
      <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
  </head>
  <body>
      <header class="tm-site-header" style="height:300px;">
           <h1 class="tm-site-name"> SuperSavers </h1>
           <nav class="navbar navbar-expand-md tm-main-nav-container">
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#tmMainNav" aria-controls="tmMainNav" aria-expanded="false" aria-label="Toggle navigation">
                       <i class="fa fa-bars"></i>
               </button>
               <div class="collapse navbar-collapse tm-main-nav" id="tmMainNav">
                   <ul class="nav nav-fill tm-main-nav-ul">
                       <li class="nav-item"><a class="nav-link" href="/"> Accueil </a></li>
                       <li class="nav-item"><a class="nav-link" href="/SignUp"> Proposer mes services </a></li>
                   </ul>
               </div>
           </nav>
      </header>

      <div class="tm-main-content">
          <section class="row tm-margin-b-l">
              <div class="col-12">
                  <p class="tm-blue-text tm-margin-b-p">
                    Vous demander de l&#39aide pour : <span style="color:#C80910"> <c:out value="${ incidentType }"/> </span> <br/>
                    Complétez certaines informations pour trouver un héro disponible :
                  </p>
              </div>
              <div class="col-md-6 col-sm-12 mb-md-0 mb-5 tm-overflow-auto">
                  <div class="mr-lg-5">
                      <form action="/HeroSearch" method="post" class="tm-contact-form">
                          <div class="form-group">
                              <input type="text" id="city" name="city" class="form-control" placeholder="Votre ville"  required/>
                          </div>
                          <div class="form-group">
                              <input type="text" id="latitude" name="latitude" class="form-control" placeholder="Votre latitude"  required/>
                          </div>
                          <div class="form-group">
                              <input type="text" id="longitude" name="longitude" class="form-control" placeholder="Votre longitude"  required/>
                          </div>

                          <div id="detailsMap"></div>

                          <button type="submit" class="tm-btn tm-btn-blue float-right" onclick="getCityInfo"> Envoyer </button>
                      </form>
                  </div>
              </div>
              <div class="col-md-6 col-sm-12">
                  <div class="tm-gallery" style="padding-top: 0px">
                        <div class="row">
                            <c:forEach var="hero" items="${ heroes }">
                              <figure class="col-lg-3 col-md-4 col-sm-6 col-12 tm-gallery-item">
                                  <p class="tm-figcaption">
                                      <c:out value="${ hero.name }" />
                                      </br>
                                      <c:out value="Tel : ${ hero.phone }" />
                                  </p>
                              </figure>
                            </c:forEach>
                        </div>
                    </div>
              </div>
          </section>
       </div>
       <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
       <script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>
       <script type="text/javascript">

           var villes = {
            	"Paris": { "lat": 48.852969, "lon": 2.349903 },
            	"Brest": { "lat": 48.383, "lon": -4.500 },
            	"Quimper": { "lat": 48.000, "lon": -4.100 },
            	"Bayonne": { "lat": 43.500, "lon": -1.467 }
           };

           // On initialise la latitude et la longitude de Paris (centre de la carte)
           var lat = 48.852969;
           var lon = 2.349903;
           var macarte, marqueur;

           var markerClusters; // Servira à stocker les groupes de marqueurs

           // Fonction d'initialisation de la carte
           function initMap() {
                var markers = []; // Nous initialisons la liste des marqueurs

           	    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
           	    macarte = L.map('detailsMap').setView([lat, lon], 11);

           	    markerClusters = L.markerClusterGroup(); // Nous initialisons les groupes de marqueurs

           	    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
                L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                    // Il est toujours bien de laisser le lien vers la source des données
                    attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
                    minZoom: 1,
                    maxZoom: 20
                }).addTo(macarte);

           	    // Nous parcourons la liste des villes
                for (ville in villes) {
                    var marker = L.marker([villes[ville].lat, villes[ville].lon]); // pas de addTo(macarte), l'affichage sera géré par la bibliothèque des clusters
                    marker.bindPopup(ville);
                    markerClusters.addLayer(marker); // Nous ajoutons le marqueur aux groupes
                    markers.push(marker); // Nous ajoutons le marqueur à la liste des marqueurs
                }

                var group = new L.featureGroup(markers); // Nous créons le groupe des marqueurs pour adapter le zoom
                macarte.fitBounds(group.getBounds().pad(0.5)); // Nous demandons à ce que tous les marqueurs soient visibles, et ajoutons un padding (pad(0.5)) pour que les marqueurs ne soient pas coupés
                macarte.addLayer(markerClusters);
           }

           window.onload = function(){
                // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
                initMap();
                macarte.on('click', mapClickListen);
           };

           function mapClickListen(e) {
               // On récupère les coordonnées du clic
               let pos = e.latlng

               // On crée un marqueur
               addMarker(pos)

               // On affiche les coordonnées dans le formulaire
               document.querySelector("#latitude").value=pos.lat
               document.querySelector("#longitude").value=pos.lng
           }

           function addMarker(pos){
               // On vérifie si le marqueur existe déjà
               if (marqueur != undefined) {
                   // Si oui, on le retire pour n'avoir qu'un seul marqeur créé lorsque je clique, sinon ça fera plein de marqeurs partout jusqu'à celui que je clique en dernier
                   macarte.removeLayer(marqueur);
               }

               // On crée le marqueur aux coordonnées "pos"
               marqueur = L.marker(pos)

               // On ajoute le marqueur
               marqueur.addTo(macarte)
           }

           function getCityInfo(){
               var ville = $("#city").val();
               if(ville != ""){
                   $.ajax({
                       url: "https://nominatim.openstreetmap.org/search", // URL de Nominatim
                       type: 'get', // Requête de type GET
                       data: "q="+ville+"&format=json&addressdetails=1&limit=1&polygon_svg=1" // Données envoyées (q -> adresse complète, format -> format attendu pour la réponse, limit -> nombre de réponses attendu, polygon_svg -> fournit les données de polygone de la réponse en svg)
                   }).done(function (response) {
                       if(response != ""){
                           userlat = response[0]['lat'];
                           userlon = response[0]['lon'];
                       }
                   }).fail(function (error) {
                       alert(error);
                   });
               }
           }

           function cercle(){ // Ou tout autre nom de fonction
               // TODO en km ou m ici ? 50 ou 50000 ?
               var distance = 50;
               $.ajax({
                   url:'https://',
                   type: 'POST',
                   data: 'lat='+userlat+'&lon='+userlon+'&distance='+distance
               }).done(function(reponse){
                   var points = JSON.parse(reponse);
                   // Ici nous traitons les différents points
               }).fail (function(error){
                   console.log(error);
               });
           }

       </script>
  </body>
</html>

<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
  <head>
      <title> Merci ! </title>
      <meta charset="utf-8"/>
      <link rel="stylesheet" href="/css/bootstrap.min.css">
      <link rel="stylesheet" href="/css/tooplate-style.css">
      <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat:700' rel='stylesheet' type='text/css'>
      <style>
        @import url(//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css);
        @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
      </style>
      <link rel="stylesheet" href="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/default_thank_you.css">
      <script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/jquery-1.9.1.min.js"></script>
      <script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/html5shiv.js"></script>
  </head>
  <body>
      <header class="tm-site-header" style="height:300px;">
              <h1 class="tm-site-name" style="text-align: left"> SuperSavers </h1>
              <nav class="navbar navbar-expand-md tm-main-nav-container">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#tmMainNav" aria-controls="tmMainNav" aria-expanded="false" aria-label="Toggle navigation">
                          <i class="fa fa-bars"></i>
                  </button>
                  <div class="collapse navbar-collapse tm-main-nav" id="tmMainNav">
                      <ul class="nav nav-fill tm-main-nav-ul">
                          <li class="nav-item"><a class="nav-link" href="/"> Accueil </a></li>
                          <li class="nav-item"><a class="nav-link" href="/SignUp"> Proposer mes services </a></li>
                      </ul>
                  </div>
              </nav>
      </header>
      <div class="site-header" id="header">
        <h1 class="site-header__title" data-lead-id="site-header-title" style="font-size:40px; color:#C80910"> <c:out value="${ message }"/> </h1>
      </div>
  </body>
</html>
